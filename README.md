# Infused Multi-Project Folder Structure for Unity3D #

These instructions contain all the information about the 'Infused Multi-Project Folder Structure for Unity3D'.

Last edited on: **28-01-2018** _(dd-mm-yyyy)_

---

## Table of Contents ##

- Introduction
- Use Cases
- Installation
- Folder Structure
- Developed by Infused

---

## Introduction ##

This folder structure contains pre-defined folder structures based on Unity conventional folder names.
The purpose of this is to make (complex) projects easier to maintain.

---

## Use Cases ##

This folder structure is very flexible and can be used when:

1. You have multiple projects inside your Unity project.
    - Example: You have 4 different games/experiences which are built into 1 Unity3D project. This way your users only have to buy or download 1 app which contains multiple games/experiences.
2. You want to kickstart your project.

---

## Installation ##

1. Create a **new** Unity3D project.
2. Navigate to the **'Assets'** folder of your newly created project.
3. Copy/Paste the folder structure inside the **'Assets'** folder.
4. You're done! ^^

---

## Folder Structure ##

**Note**:  
_Don't let this folder structure get in your way! Use and change it to fit your project's needs!_

### Main Folder Structure ###

- **_Global-Resources**
    - _Contains all global resources which are shared througout your project._
    - _Folder name is prefixed with an underscore so it gets listed at the top for easy access._
- **Project-Template**
    - _Contains all resources which are specific to a project._
    - _Tip: Duplicate and rename this folder according to your project to easily add more projects later on._

### Subfolder Structure ###

Both the **'Global-Resources'** and the **'Project-Template'** folders contain the same folder structure:

- **3rd-Party**
    - _Stores all 3rd party resources which are NOT downloaded through the Asset Store._
- **Animations**
    - _Folder for storing your Unity3D animations._
- **Asset-Store**
    - _Folder for storing all Asset Store downloads. This makes it easier to find and update these assets._
    - _This also seperates your own files from Asset Store files so you always know which files are yours._
- **Audio**
    - **Music**
        - _Folder for storing your music & soundtracks._
    - **SFX**
        - _Folder for storing your sound effects._
- **Materials**
    - _Folder for storing your materials._
- **Models**
    - _Folder for storing your 3D models._
- **Plugins**
    - _Folder for storing your plugins._
- **Prefabs**
    - _Folder for storing your Prefabs._
- **Scenes**
    - _Folder for storing your scenes._
- **Scripts**
    - _Folder for storing your scripts._
- **Shaders**
    - _Folder for storing your shaders._
- **Textures**
    - _Folder for storing your textures._

---

## Developed by Infused ##

**Infused**(.nl)  
Christiaan Bruinsma  
[Christiaan@infused.nl](mailto:christiaan@infused.nl)
